package sentry

import (
	"bitbucket.org/bertimus9/systemstat"
	"sentry/metrics"
	"runtime"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func collectMemStat()  {
	memStat := systemstat.GetMemSample()
	memMetric := metrics.MemoryMetric{
		FreeMemory: float64(memStat.MemFree),
		UsedMemory: float64(memStat.MemUsed),
		Time: memStat.Time,
	}

	Submit(memMetric)
}