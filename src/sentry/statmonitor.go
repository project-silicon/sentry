package sentry

import (
	"sentry/metrics"
	"sentry/alert"
)

func monitorStat()  {
	for _, ts := range metrics.TimeSeries {
		if (ts == metrics.ResponseTime || ts == metrics.CpuUsage ||
			ts == metrics.MemoryUsage || ts == metrics.DiskUsage) {
			// get data points by series name
			dp := getDataPointsByGraph(ts)
			// process data per series
			processDataPoints(dp, ts)
		}
	}
}

func processDataPoints(dataPoints []*metrics.DataPoint, graph string) {
	// create a blank map for metric name + data
	dataMap := make(map[string][]float64)
	// iterate data points
	for _, dp := range dataPoints {
		// if metric name exists
		if slice, ok := dataMap[dp.Name]; ok {
			// append the data
			dataMap[dp.Name] = append(slice, dp.Data)
		} else {
			// create a new data array and put into the map
			// with the key as metric name
			dataMap[dp.Name] = []float64{dp.Data}
		}
	}

	// iterate the name data map
	for name := range dataMap {
		// get the data array
		data := dataMap[name]
		length := len(data)
		// if data array has more than one data
		if length > 1 {
			// take the most recent data
			// array is already sorted against timestamp while querying
			// from db via getDataPointsByGraph()
			recent := data[0]
			// get he average of rest of the data
			avg := average(data[1:])
			// get percent increase
			percentIncrease := ((recent - avg) / avg) * 100

			if graph == metrics.ResponseTime {
				if percentIncrease > config.MonitorConfig.HttpThreshold {
					raiseAlert(percentIncrease, recent, avg, name, graph)
				}
			} else if graph == metrics.CpuUsage {
				if percentIncrease > config.MonitorConfig.CpuThreshold {
					raiseAlert(percentIncrease, recent, avg, name, graph)
				}
			} else if graph == metrics.MemoryUsage {
				if percentIncrease > config.MonitorConfig.MemoryThreshold {
					raiseAlert(percentIncrease, recent, avg, name, graph)
			 }
			} else if graph == metrics.DiskUsage {
				if percentIncrease > config.MonitorConfig.DiskThreshold {
					raiseAlert(percentIncrease, recent, avg, name, graph)
				}
			}
		}
	}
}

func average(data ...float64) float64 {
	total:=0.0
	for _,v:=range data {
		total +=v
	}
	return total/float64(len(data))
}

func raiseAlert(percentIncrease, recent, avg float64, name, graph string)  {
	message := "Alert for " + graph + ": " + name + " has increased by %f%%."
	message = message + " Currrent value is - %f, where average is - %f"
	alert.Notify(nil, message, percentIncrease, recent, avg)
}