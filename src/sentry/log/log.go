package log

import (
	"github.com/Sirupsen/logrus"
	"github.com/sebest/logrusly"
)

var logger 	*logrus.Logger
var hook 	*logrusly.LogglyHook

type Context map[string]interface{}

// Configure log package by passing loggly api token
func Configure(apiToken string)  {
	// initialize loggly client
	logger = logrus.New()
	hook = logrusly.NewLogglyHook(apiToken, "https://logs-01.loggly.com/bulk/", logrus.DebugLevel, "silicon", "engine")
	logger.Hooks.Add(hook)
}

func Debug(message string, args ...interface{}) {
	DebugC(nil, message, args...)
}

func DebugC(context Context, message string, args ...interface{}) {
	logger.WithFields(logrus.Fields(context)).Debugf(message + "\n", args...)
}

func Info(message string, args ...interface{}) {
	InfoC(nil, message, args...)
}

func InfoC(context Context, message string, args ...interface{}) {
	logger.WithFields(logrus.Fields(context)).Infof(message + "\n", args...)
}

func Error(message string, args ...interface{}) {
	ErrorC(nil, message, args...)
}

func ErrorC(context Context, message string, args ...interface{}) {
	logger.WithFields(logrus.Fields(context)).Errorf(message + "\n", args...)
}

func Print(message string, args ...interface{}) {
	PrintC(nil, message, args...)
}

func PrintC(context Context, message string, args ...interface{}) {
	logger.WithFields(logrus.Fields(context)).Printf(message + "\n", args...)
}

func Flush()  {
	hook.Flush()
}

// TODO: create a controller to set log level
// Sets log level dynamically
func SetLevel(level string)  {
	l, err := logrus.ParseLevel(level)
	if err != nil {
		Info("Wrong log level specified %s", level)
		return
	}
	logger.Level = l
}

func GetLevel() string {
	return logger.Level.String()
}

