package sentry

import "gopkg.in/mgo.v2"

// Sentry configuration structure
type SentryConf struct {
	// Application name
	AppName		string
	// Ring buffer size for a metric channel
	BufferSize 	int
	// Collection rate in cron format
	CollectionRate 	string
	// Sampling rate in cron format
	SamplingRate	string
	// Mongo db session
	Session 	*mgo.Session
	// Http Post url to post metrics to remote sentry
	PostUrl		string
	// Indicates if a sentry instance is remote or local
	IsRemote	bool
	// Slack api token
	SlackApiToken	string		//xoxb-23232923333-Dvsfb2pWmsjzDRgZcYhbYcWO
	// Loggly api token
	LogglyApiToken	string		//417c5f56-6bb5-4145-a6f5-217a16533ab4
	//Threshold values to monitor metrics
	MonitorConfig 	*MonitorConfig
}

type MonitorConfig struct {
	HttpThreshold		float64
	CpuThreshold		float64
	MemoryThreshold		float64
	DiskThreshold		float64
}