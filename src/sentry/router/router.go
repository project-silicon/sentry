package router

import (
	"net/http"
	"github.com/gorilla/mux"
	"time"
	"sentry/metrics"
	"sentry"
)

// Route structure which helps to collect http metric
type Route struct {
	// Name of the route
	Name        string
	// Http method
	Method      string
	// Url pattern
	Pattern     string
	// Http handler function
	HandlerFunc http.HandlerFunc
}

// collection of routes
var routes []Route

func init()  {
	// create routes for sentry operations
	routes = []Route{
		Route{
			Name: "PostMetrics",
			Method: "POST",
			Pattern: sentry.PostPath,
			HandlerFunc: sentry.PostHandler,
		},
		Route{
			Name: "GetDataPoints",
			Method: "GET",
			Pattern: sentry.GetDataPointsPath,
			HandlerFunc: sentry.GetDataPointsHandler,
		},
		Route{
			Name: "GetAllDataPoints",
			Method: "GET",
			Pattern: sentry.GetAllPath,
			HandlerFunc: sentry.GetAllDataPointsHandler,
		},
		Route{
			Name: "GetTimeSeries",
			Method: "GET",
			Pattern: sentry.GetTimeSeriesPath,
			HandlerFunc: sentry.GetTimeSeriesHandler,
		},
	}
}

// Registers a new route
func RegisterRoute(route Route) {
	routes = append(routes, route)
}

// Initialize a new router
func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler

		handler = route.HandlerFunc

		// creates a loggable wrapper http handler
		handler = loggableHandler(handler, route.Name)

		router.Methods(route.Method).
		Path(route.Pattern).
		Name(route.Name).
		Handler(handler)
	}
	return router
}

// Creates a loggable http handler wrapping around a supplied http handler.
// This handler logs the response time for each http request.
func loggableHandler(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// get start time
		start := time.Now()

		// serve the actual http request
		inner.ServeHTTP(w, r)

		// calculate the response time
		responseTime := time.Since(start)

		// log the response time
		metric := metrics.HttpMetric {
			Method: r.Method,
			URI: r.RequestURI,
			Name: name,
			ResponseTime: responseTime,
		}
		// submit the metric to sentry
		sentry.Submit(metric)
	})
}