package sentry

import (
	"bitbucket.org/bertimus9/systemstat"
	"sentry/metrics"
)

func collectCpuStat()  {
	cpuStat := systemstat.GetCPUSample()

	cpuMetric := metrics.CPUMetric{
		TotalTime: float64(cpuStat.Total),
		IdleTime: float64(cpuStat.Idle),
		Time: cpuStat.Time,
	}

	Submit(cpuMetric)
}