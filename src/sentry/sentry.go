package sentry

import (
	"sentry/metrics"
	"sentry/alert"
	"sentry/log"
)

var config *SentryConf

// Configures sentry
func Configure(conf *SentryConf) {
	ringBufferSize = conf.BufferSize
	samplingRate = conf.SamplingRate
	collectionRate = conf.CollectionRate
	config = conf
	configDb(conf.Session)
	alert.Configure(conf.SlackApiToken)
	log.Configure(conf.LogglyApiToken)
}

// Submits metrics to sentry
func Submit(metrics ...metrics.Metric) {
	go submit(metrics...)
}

// Starts the metrics collector
func Start()  {
	startCollector()
}

// Stops the metrics collector
func Stop()  {
	stopCollector()
}