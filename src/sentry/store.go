package sentry

import (
	"sentry/metrics"
	"gopkg.in/mgo.v2"
	"sentry/log"
	"gopkg.in/mgo.v2/bson"
)

var session *mgo.Session
const NewsLetterDb		= "newsletter"


// Configures mongo db session
func configDb(dbSession *mgo.Session) {
	if dbSession != nil {
		session = dbSession.Copy()
	}
	err := ensureIndices(session)
	if err != nil {
		log.Error("Failed to ensure indices. Cause %v", err)
		panic(err)
	}
}

// Ensures indices for series, time and name column for indexed search
func ensureIndices(session *mgo.Session) error {
	index := mgo.Index{
		Key:        []string{"series", "name", "time"},
		Unique:     false,
		DropDups:   false,
		Background: true,
		Sparse:     false,
	}

	copy := session.Copy()
	defer copy.Close()

	c := copy.DB(NewsLetterDb).C(metrics.CpuUsage)
	err := c.EnsureIndex(index)
	if err != nil {
		return err
	}

	c = copy.DB(NewsLetterDb).C(metrics.GcStat)
	err = c.EnsureIndex(index)
	if err != nil {
		return err
	}

	c = copy.DB(NewsLetterDb).C(metrics.MemoryUsage)
	err = c.EnsureIndex(index)
	if err != nil {
		return err
	}

	c = copy.DB(NewsLetterDb).C(metrics.ResponseTime)
	err = c.EnsureIndex(index)
	if err != nil {
		return err
	}

	c = copy.DB(NewsLetterDb).C(metrics.Runtime)
	err = c.EnsureIndex(index)
	if err != nil {
		return err
	}

	c = copy.DB(NewsLetterDb).C(metrics.LoadCount)
	err = c.EnsureIndex(index)
	if err != nil {
		return err
	}

	c = copy.DB(NewsLetterDb).C(metrics.DiskUsage)
	err = c.EnsureIndex(index)
	if err != nil {
		return err
	}

	return nil
}

// Save a data point to mongo db
func save(name string, dataPoint *metrics.DataPoint)  {
	copy := session.Copy()
	defer copy.Close()

	copy.DB(NewsLetterDb).C(name).Insert(dataPoint)
}

func getDataPointsByName(graph, name string) []*metrics.DataPoint {
	copy := session.Copy()
	defer copy.Close()

	var dataPoints []*metrics.DataPoint
	query := bson.M{"name": name}

	copy.DB(NewsLetterDb).C(graph).Find(query).Sort("-time").All(&dataPoints)
	return dataPoints
}

func getDataPointsByGraph(graph string) []*metrics.DataPoint {
	copy := session.Copy()
	defer copy.Close()

	var dataPoints []*metrics.DataPoint
	copy.DB(NewsLetterDb).C(graph).Find(nil).Sort("-time").All(&dataPoints)
	return dataPoints
}