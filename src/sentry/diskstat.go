package sentry

import (
	"syscall"
	"os"
	"sentry/log"
	"sentry/metrics"
	"time"
)

func collectDiskStat()  {
	var stat syscall.Statfs_t
	wd, err := os.Getwd()

	if err != nil {
		log.Error("Failed to get disk usage state. Cause %v", err)
		return
	}

	syscall.Statfs(wd, &stat)

	free := stat.Bavail * uint64(stat.Bsize)

	diskMetric := &metrics.DiskMetric{
		FreeDiskSpace: float64(free),
		Time: time.Now(),
	}

	Submit(diskMetric)
}
