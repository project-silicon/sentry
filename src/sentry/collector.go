package sentry

import (
	"sentry/metrics"
	"gopkg.in/eapache/channels.v1"
	"github.com/robfig/cron"
	"errors"
	"encoding/json"
	"sentry/log"
	"net/http"
	"bytes"
)

var channelMap map[string]*channels.RingChannel
var scheduler *cron.Cron
var ringBufferSize = 2
var samplingRate = "@every 10s"
var collectionRate = "@every 1m"
var archiveRate	= "@midnight"

func init() {
	scheduler = cron.New()
	channelMap = make(map[string]*channels.RingChannel)
}

// Submits the metrics to a new ring buffered channel
// It creates a channel with the name of the data point.
//
// It pushes the data to the ring buffered channel overwriting
// the old values. Later a scheduler will pick the most recent
// metric value from the channel after a certain interval of time.
func submit(metrics ...metrics.Metric) {
	for _, metric := range metrics {
		if metric != nil {
			// create data point from metric
			dataPoints := metric.DataPoints(config.AppName)

			for _, dataPoint := range dataPoints {
				// get the name of the time series
				name := dataPoint.Name

				// if a channel exists with the name of time series
				// send the data point to the channel
				if channel, ok := channelMap[name]; ok {
					channel.In() <- dataPoint
				} else {
					// otherwise, create a new ring buffered channel
					// and put it into the channel map with the name
					// of the data point and send the data point to
					// the channel
					size := channels.BufferCap(ringBufferSize)
					channel = channels.NewRingChannel(size)
					channel.In() <- dataPoint
					channelMap[name] = channel
				}
			}
		}
	}
}

// It collects metrics from the channels. If this is a remote setup, then
// retrieved metrics will be posted to remote sentry instance. If current
// setup is a local setup and connected to a mongo db instance, then it will
// persists the metrics.
func collect() {
	// if set as remote but post url is not provided
	if config.IsRemote && config.PostUrl == "" {
		err := errors.New("Http post url is not setup.")
		log.Error("Error while submitting metrics, %v", err)
		return
	}

	// if set as local but mongo db is not setup
	if !config.IsRemote && config.Session == nil {
		err := errors.New("Local mongo db is not setup.")
		log.Error("Error while submitting metrics, %v", err)
		return
	}

	for _, channel := range channelMap {
		// retrieve data point from each channel
		val := <-channel.Out()
		dataPoint := val.(*metrics.DataPoint)
		if (!config.IsRemote) {
			// if local, save it to local db
			go save(dataPoint.Graph, dataPoint)
		} else {
			// if remote post it to remote sentry
			go post(dataPoint)
		}
	}
}

// Post metrics to remote sentry instance specified by config.PostUrl
func post(dataPoint *metrics.DataPoint) {
	// marshal the data point
	jsonStr, err := json.Marshal(dataPoint)

	if err != nil {
		log.Error("Error while marshalling metrics, %v", err)
		return
	}

	// create a http post request
	req, err := http.NewRequest("POST", config.PostUrl + PostPath, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	// post the request
	resp, err := client.Do(req)

	if err != nil {
		log.Error("Error while posting metrics, %v", err)
	}
	defer resp.Body.Close()
}

// Starts the collector scheduler
func startCollector() {
	scheduleJobs()
	scheduler.Start()
}

// Stops the collector schedulers
func stopCollector() {
	scheduler.Stop()
}



