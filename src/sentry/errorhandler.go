package sentry

import (
	"sentry/log"
	"sentry/alert"
)

// Recovers the program from a panic
func HandlePanic(location string) {
	// recover from a panic
	if err := recover(); err != nil {
		ctx := alert.Notification{
			AlertLevel:     alert.Critical,
			ErrorCode: 	alert.ErrorUnhandled,
			PackageName: "handler",
			MethodName: "HandlePanic",
			Context: log.Context{
				"Location": location,
				"Error": err,
			},
		}
		alert.Notify(ctx, "Unhandled error occurred at " + location + " - %v", err)
	}
}