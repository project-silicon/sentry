package alert

import (
	"sentry/log"
	"github.com/bluele/slack"
	"fmt"
)

//https://bitbucket.org/bertimus9/systemstat/src
const (
// slack error channel
	ErrorChannel = "silicon-error"
// slack critical channel
	CriticalChannel = "silicon-critical"
// slack alert
	AlertChannel = "silicon-alert"
)

type ErrorCode string

const (
	ErrorUnhandled			ErrorCode = "SI00001"
	ErrorJsonEncoding		ErrorCode = "SI00002"
	ErrorThresholdReached		ErrorCode = "SI00003"
)

// Alert level
type Level uint8

// Notification structure
type Notification struct {
	// Notification alert level
	AlertLevel  Level
	// Error code
	ErrorCode   ErrorCode
	// Package name of alert location
	PackageName string
	// Method name of alert location
	MethodName  string
	// Other context data as map
	Context     log.Context
}

const (
	Error Level = iota
	Critical
)

var api *slack.Slack
var errorChannel *slack.Channel
var criticalChannel *slack.Channel
var alertChannel *slack.Channel

// Configures alert by passing slack api token
func Configure(apiToken string)  {
	var err error
	api = slack.New(apiToken)
	errorChannel, err = api.FindChannelByName(ErrorChannel)
	if err != nil {
		log.Error("Failed to get silicon-error channel for Slack. Error - %v", err)
	}
	criticalChannel, err = api.FindChannelByName(CriticalChannel)
	if err != nil {
		log.Error("Failed to get silicon-critical channel for Slack. Error - %v", err)
	}
	alertChannel, err = api.FindChannelByName(AlertChannel)
	if err != nil {
		log.Error("Failed to get silicon-alert channel for Slack. Error - %v", err)
	}

}

// Create alerts
func Notify(notification Notification, message string, args ...interface{}) {
	if notification != nil {
		go notifyInternal(notification, message, args...)
	} else {
		go raiseAlert(message, args...)
	}
}

// Sends a message in proper slack channel based on AlertLevel and sends a loggly
// message also with all the context information
func notifyInternal(notification Notification, message string, args ...interface{}) {
	// get the error code
	errorCode := string(notification.ErrorCode)
	// create the log message
	msg := errorCode + fmt.Sprintf(message, args...)
	// populate the context from notification
	ctx := notification.Context

	if notification.AlertLevel == Critical {
		// if critical level, send slack message to critical channel
		api.ChatPostMessage(criticalChannel.Id, msg, nil)
		ctx["Severity"] = "Critical"
	} else {
		// otherwise, send slack message to error channel
		api.ChatPostMessage(errorChannel.Id, msg, nil)
		ctx["Severity"] = "Error"
	}
	// populate the context with other values
	ctx["ErrorCode"] = errorCode
	ctx["Method"] = notification.MethodName
	ctx["Package"] = notification.PackageName

	// send loggly message
	log.ErrorC(ctx, message, args...)
}

func raiseAlert(message string, args ...interface{}) {
	msg := fmt.Sprintf(message, args...)
	api.ChatPostMessage(alertChannel.Id, msg, nil)
}