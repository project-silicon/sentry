package sentry

import (
	"runtime"
	"time"
	"sentry/metrics"
)

var lastSampleTime time.Time
var lastPauseNs uint64 = 0
var lastNumGc uint32 = 0
var nsInMs float64 = float64(time.Millisecond)

func collectGcStat()  {
	var mem runtime.MemStats
	runtime.ReadMemStats(&mem)

	now := time.Now()

	var gcPausePerSecond float64

	if lastPauseNs > 0 {
		pauseSinceLastSample := mem.PauseTotalNs - lastPauseNs
		gcPausePerSecond = float64(pauseSinceLastSample) / nsInMs
	}

	lastPauseNs = mem.PauseTotalNs

	countGc := int(mem.NumGC - lastNumGc)

	var gcPerSecond float64

	if lastNumGc > 0 {
		diff := float64(countGc)
		diffTime := now.Sub(lastSampleTime).Seconds()
		gcPerSecond = diff / diffTime
	}

	if countGc > 256 {
		// lagging GC pause times
		countGc = 256
	}

	lastNumGc = mem.NumGC
	lastSampleTime = time.Now()

	gcStat := metrics.GCMetric{
		Time: now,
		GoRoutineNum: float64(runtime.NumGoroutine()),
		GoMaxProcs: float64(runtime.GOMAXPROCS(0)),
		HeapAlloc: float64(mem.HeapAlloc),
		HeapSys: float64(mem.HeapSys),
		HeapIdle: float64(mem.HeapIdle),
		HeapInUse: float64(mem.HeapInuse),
		HeapReleased: float64(mem.HeapReleased),
		HeapObjects: float64(mem.HeapObjects),
		GcNum: float64(mem.NumGC),
		GcPerSecond: float64(gcPerSecond),
		GcPausePerSecond: float64(gcPausePerSecond),
	}

	Submit(gcStat)
}