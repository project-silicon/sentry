package sentry


// Schedules different jobs
func scheduleJobs() {
	// schedules collector job
	scheduler.AddFunc(collectionRate, collect)

	// schedules stat collection job
	scheduler.AddFunc(samplingRate, collectMemStat)
	scheduler.AddFunc(samplingRate, collectCpuStat)
	scheduler.AddFunc(samplingRate, collectGcStat)
	scheduler.AddFunc(samplingRate, collectDiskStat)

	if(!config.IsRemote) {
		// run compact and monitor alert job
		scheduler.AddFunc(collectionRate, monitorStat)
		scheduler.AddFunc(archiveRate, archiveJob)
	}
}

