package metrics

import (
	"time"
	"gopkg.in/mgo.v2/bson"
)

// Name for mongo different metric channel and corresponding
// mongo db collections
const (
	ResponseTime	string = "ResponseTime"
	LoadCount	string = "LoadCount"
	CpuUsage	string = "CPUUsage"
	MemoryUsage	string = "MemoryUsage"
	GcStat		string = "GC"
	Runtime		string = "Runtime"
	DiskUsage	string = "DiskUsage"
)

// Metric interface, which provides data point for a time series
type Metric interface {
	DataPoints(appName string) []*DataPoint
}

var TimeSeries = []string {
	ResponseTime,
	CpuUsage,
	MemoryUsage,
	GcStat,
	Runtime,
	LoadCount,
	DiskUsage,
}

// Data point structure
type DataPoint struct {
	// Id for mongo db
	Id      	bson.ObjectId			`bson:"_id,omitempty" json:"-"`
	// Application name
	AppName		string				`bson:"appName" json:"appName"`
	// Time series name
	Graph		string				`bson:"series" json:"series"`
	// Identifier of the metric
	Name		string				`bson:"name" json:"name"`
	// Data component
	Data 		float64				`bson:"data" json:"data"`
	// Time component
	Time		time.Time			`bson:"time" json:"time"`
	// Context related information of the data point
	Context		map[string]interface{}		`bson:"context" json:"context"`
}


