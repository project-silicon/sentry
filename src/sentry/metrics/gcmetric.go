package metrics

import (
	"time"
	"os"
)

type GCMetric struct {
	// Number of go routines
	GoRoutineNum     float64
	// Number of processor cores used
	GoMaxProcs       float64
	// Bytes allocated but not freed
	HeapAlloc        float64
	// Bytes obtained from system
	HeapSys          float64
	// Bytes in idle span
	HeapIdle         float64
	// Bytes in non-idle span
	HeapInUse        float64
	// Bytes released to OS
	HeapReleased     float64
	// Total number of allocated objects
	HeapObjects      float64
	// Number of GC runs
	GcNum            float64
	// Number of GC per second
	GcPerSecond      float64
	// Pause duration by GC per second
	GcPausePerSecond float64
	// Current timestamp
	Time             time.Time
}

func (this GCMetric) DataPoints(appName string) []*DataPoint {
	hostname, _ := os.Hostname()

	runtimeData := &DataPoint{
		AppName: appName,
		Graph: Runtime,
		Data: this.GoRoutineNum,
		Time: this.Time,
		Name: "NumGoRoutine",
		Context: map[string]interface{}{
			"Host": hostname,
			"MaxProc": this.GoMaxProcs,
		},
	}

	heapAlloc := &DataPoint{
		AppName: appName,
		Graph: GcStat,
		Data: this.HeapAlloc / (1024 * 1024),		// unit is MB
		Time: this.Time,
		Name: "HeapAllocated",
		Context: map[string]interface{}{
			"Host": hostname,
		},
	}

	heapSys := &DataPoint{
		AppName: appName,
		Graph: GcStat,
		Data: this.HeapSys / (1024 * 1024),
		Time: this.Time,
		Name: "SystemBytes",
		Context: map[string]interface{}{
			"Host": hostname,
		},
	}

	heapIdle := &DataPoint{
		AppName: appName,
		Graph: GcStat,
		Data: this.HeapIdle / (1024 * 1024),
		Time: this.Time,
		Name: "IdleBytes",
		Context: map[string]interface{}{
			"Host": hostname,
		},
	}

	heapInUse := &DataPoint{
		AppName: appName,
		Graph: GcStat,
		Data: this.HeapInUse / (1024 * 1024),
		Time: this.Time,
		Name: "UsedBytes",
		Context: map[string]interface{}{
			"Host": hostname,
		},
	}

	heapReleased := &DataPoint{
		AppName: appName,
		Graph: GcStat,
		Data: this.HeapReleased / (1024 * 1024),
		Time: this.Time,
		Name: "ReleasedBytes",
		Context: map[string]interface{}{
			"Host": hostname,
		},
	}

	heapObjects := &DataPoint{
		AppName: appName,
		Graph: GcStat,
		Data: this.HeapObjects,
		Time: this.Time,
		Name: "TotalObjects",
		Context: map[string]interface{}{
			"Host": hostname,
		},
	}

	gcNum := &DataPoint{
		AppName: appName,
		Graph: GcStat,
		Data: this.GcNum,
		Time: this.Time,
		Name: "GCRuns",
		Context: map[string]interface{}{
			"Host": hostname,
		},
	}

	gcPerSecond := &DataPoint{
		AppName: appName,
		Graph: GcStat,
		Data: this.GcPerSecond,
		Time: this.Time,
		Name: "GCPerSecond",
		Context: map[string]interface{}{
			"Host": hostname,
		},
	}

	gcPause := &DataPoint{
		AppName: appName,
		Graph: GcStat,
		Data: this.GcPausePerSecond,
		Time: this.Time,
		Name: "GCPausePerSecond",
		Context: map[string]interface{}{
			"Host": hostname,
		},
	}

	return []*DataPoint{runtimeData, heapAlloc, heapSys, heapIdle, heapInUse,
		heapReleased, heapObjects, gcNum, gcPerSecond, gcPause}
}