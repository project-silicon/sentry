package metrics

import (
	"time"
	"os"
)

type CPUMetric struct {
	IdleTime	float64
	TotalTime	float64
	Time 		time.Time
}

func (this CPUMetric) DataPoints (appName string) []*DataPoint {
	hostname, _ := os.Hostname()

	cpuUsage := &DataPoint{
		AppName: appName,
		Graph: CpuUsage,
		Data: float64((this.TotalTime - this.IdleTime) * 100 / this.TotalTime),
		Time: this.Time,
		Name: "CPUUsage",
		Context: map[string]interface{} {
			"Host": hostname,
		},
	}

	return []*DataPoint{cpuUsage};
}