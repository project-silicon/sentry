package metrics

import (
	"os"
	"time"
)

type MemoryMetric struct {
	FreeMemory 	float64
	UsedMemory 	float64
	Time 		time.Time
}

func (this MemoryMetric) DataPoints (appName string) []*DataPoint  {
	hostname, _ := os.Hostname()

	freeMem := &DataPoint{
		AppName: appName,
		Graph: MemoryUsage,
		Data: float64(this.FreeMemory / 1024),
		Time: this.Time,
		Name: "FreeMemory",
		Context: map[string]interface{} {
			"Host": hostname,
		},
	}

	usedMem := &DataPoint{
		AppName: appName,
		Graph: MemoryUsage,
		Data: float64(this.UsedMemory / 1024),
		Time: this.Time,
		Name: "UsedMemory",
		Context: map[string]interface{} {
			"Host": hostname,
		},
	}

	return []*DataPoint{freeMem, usedMem};
}