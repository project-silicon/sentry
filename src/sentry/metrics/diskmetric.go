package metrics

import (
	"time"
	"os"
)

type DiskMetric struct {
	FreeDiskSpace	float64
	Time 		time.Time
}

func (this DiskMetric) DataPoints(appName string) []*DataPoint {
	hostName, _ := os.Hostname()

	dataPoint := &DataPoint{
		AppName: appName,
		Graph: DiskUsage,
		Data: this.FreeDiskSpace / (1024 * 1024 * 1024),	// free diskspace in GB
		Time: time.Now(),
		Name: "FreeDiskSpace",
		Context: map[string]interface{} {
			"Host": hostName,
		},
	}

	return []*DataPoint{dataPoint}
}
