package metrics

import (
	"time"
	"os"
	"sync"
)

type HttpMetric struct {
	Method string
	URI string
	Name string
	ResponseTime time.Duration
}

var loadCount map[string]float64
var mutex = &sync.Mutex{}

func init()  {
	loadCount = make(map[string]float64, 0)
}

// Generates data point from http metric
func (this HttpMetric) DataPoints (appName string) []*DataPoint  {
	hostname, _ := os.Hostname()

	now := time.Now()

	dataPoint := &DataPoint{
		AppName: appName,
		Graph: ResponseTime,
		Data: float64(this.ResponseTime),
		Time: now,
		Name: this.Name,
		Context: map[string]interface{} {
			"Method": this.Method,
			"URI": this.URI,
			"Host": hostname,
		},
	}

	mutex.Lock()

	loadCount[this.Name] = loadCount[this.Name] + 1
	loadMetric := &DataPoint{
		AppName: appName,
		Graph: LoadCount,
		Data: loadCount[this.Name],
		Time: now,
		Name: this.Name,
		Context: map[string]interface{} {
			"Method": this.Method,
			"URI": this.URI,
			"Host": hostname,
		},
	}

	mutex.Unlock()

	return []*DataPoint{dataPoint, loadMetric};
}
