package sentry

import (
	"sentry/metrics"
	"encoding/json"
	"net/http"
	"sentry/alert"
	"github.com/gorilla/mux"
)

// url patterns
var PostPath string = "/sentry/submit"
var GetDataPointsPath string = "/sentry/metric/{graph}/{name}"
var GetAllPath string = "/sentry/metrics/{graph}"
var GetTimeSeriesPath string = "/sentry/graphs"

type jsonErr struct {
	Code int    `json:"code"`
	Text string `json:"text"`
}

// Post handler for remote metric posting for sentry
func PostHandler(w http.ResponseWriter, r *http.Request) {
	// get the body message
	decoder := json.NewDecoder(r.Body)
	var dataPoint *metrics.DataPoint

	// decode it to a data point
	err := decoder.Decode(dataPoint)
	if (err != nil) {
		// if decoding fails send an alert
		ctx := alert.Notification{
			AlertLevel:  alert.Error,
			ErrorCode:   alert.ErrorJsonEncoding,
			PackageName: "sentry.collector",
			MethodName:  "PostHandler",
		}
		alert.Notify(ctx, "Error while encoding json message - %v", err)
	}
	// get the time series name
	name := dataPoint.Graph
	// save it to the local mongo db
	save(name, dataPoint)
}

func GetDataPointsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	graph := vars["graph"]
	name := vars["name"]

	dataPoints := getDataPointsByName(graph, name)
	writeResponse(dataPoints, w)
}

func GetAllDataPointsHandler(w http.ResponseWriter, r *http.Request)  {
	vars := mux.Vars(r)
	graph := vars["graph"]

	dataPoints := getDataPointsByName(graph, "")
	writeResponse(dataPoints, w)
}

func GetTimeSeriesHandler(w http.ResponseWriter, r *http.Request) {
	writeResponse(metrics.TimeSeries, w)
}

func writeResponse(obj interface{}, w http.ResponseWriter) {
	// set content-type header as json
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	if obj == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	objects := obj.([]interface{})

	if objects != nil && len(objects) > 0 {
		// marshall the feed with json
		body, err := json.Marshal(objects)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// if there is feed item, send 200 OK status
		w.WriteHeader(http.StatusOK)
		w.Write(body)
	} else {
		body, err := json.Marshal(jsonErr{Code: http.StatusNotFound, Text: "Not Found"})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusNotFound)
		w.Write(body)
	}
}