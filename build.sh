#!/usr/bin/env bash

# get gb tool
go get github.com/constabulary/gb/...

# build it using gb build
gb build